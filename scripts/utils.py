import smtplib
import os
from datetime import datetime
import pytz
import itertools
import pandas as pd
from email.message import EmailMessage
from dotenv import load_dotenv
from munch import munchify

load_dotenv()

def try_numeric(element):
    try:
        return int(element)
    except (ValueError):
        try:
            return float(element)
        except (ValueError):
            return element

config = munchify({k: try_numeric(v) for k, v in os.environ.items() if v != ''})


def DCut(dataframe,columns,conditions,values): #Function to more easily conditionally slice dataframe
    for n in range(len(conditions)):
        column=columns[n]
        condition=conditions[n]
        value=values[n]
        if condition=='eq':
            dataframe=dataframe[dataframe[column]==value].reset_index(drop=True)
        elif condition=='ls':
            dataframe=dataframe[dataframe[column]<value].reset_index(drop=True)
        elif condition=='gr':
            dataframe=dataframe[dataframe[column]>value].reset_index(drop=True)
        elif condition=='neq':
            dataframe=dataframe[dataframe[column]!=value].reset_index(drop=True)
        elif condition=='leq':
            dataframe=dataframe[dataframe[column]<=value].reset_index(drop=True)
        elif condition=='geq':
            dataframe=dataframe[dataframe[column]>=value].reset_index(drop=True)
        else: 
            print('Condition not understood, no cuts made...')
    return dataframe

def D_Essemble(dataframe,columns):
    # the line below would create a cartesan product of all of the values in each of the
    # columns of listed in the 'columns' variable.  This creates a list of all of the possible
    # combinations of each column.  However, this approach could create a combination that 
    # does not exist in the dataset.  For example, if the two columns are race and sex, 
    # one of the possible combinations could be ('Hispanic', 'F'), but there may not be any hispanic
    # females in the dataset.  In that situation, it could create a problem when plotting the data
    # because the the chart will place that category on the x axis, but not have any data to plut
    # this tends to throw an error in the plotting algo.
    #
    # values=list(itertools.product(*[dataframe[column].dropna().unique().tolist() for column in columns]))
    # 
    # alternatively, the following approach will make a list of only the combinations that actually
    # exist in the dataset, and, therefore avoids that potential error.
    # 
    values=list(map(tuple, pd.array(dataframe[columns].dropna().to_records(index=False)).unique()))
    d_enssemble={}
    for value in values:
        # the next two lines are no longer needed with the above approach to the values array.
        # these two lines were intended to find any situations where a particular combination
        # did not exist in the dataset.  Now, since only the combinations that exist in the 
        # dataset are being used, there will never be a situation where the len(df) would be zero
        # 
        # df=DCut(dataframe,columns,['eq']*len(columns),value)
        # if len(df)>0:
        d_enssemble[value]=DCut(dataframe,columns,['eq']*len(columns),value)
    return d_enssemble


def Email_PDF(user_email, password, subject, attachments, addresses):
    server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
    server.ehlo()
    server.login(user_email, password)

    email = EmailMessage()
    email['Subject'] = subject

    for attachment in attachments:
        with open(attachment.fileLocation, 'rb') as f:
            email.add_attachment(
                f.read(),
                maintype='application',
                subtype=attachment.subType,
                filename=os.path.basename(attachment.fileLocation)
            )

    server.sendmail(user_email,addresses,email.as_string())
    

def Error_Log(step,trbck,stime,errfile,mxfsize=100):
    ftime=datetime.now(pytz.timezone(config.timezone))

    with open(errfile,'a+') as f:
        fsize=os.path.getsize(errfile)/1e3
        if fsize < mxfsize:
            f.write('#'*30+'\n')
            f.write('Program failed after '+str(round((ftime-stime).seconds/60,2))+' minutes, on '+ftime.strftime('%b %d, %Y %H:%M %p')+' during '+step+' due to the following reason(s): \n')
            f.write('\n'+str(trbck))
            f.write('#'*30+'\n') 
            f.write('\n\n\n')
        else:
            print('Error log is larger than max size. Change max size or clear error log')
